import * as expect from "expect";
import "mocha";
import { FedranHyphenPlugin } from "../fedran-hyphen";

describe("parsing", function()
{
    let plugin = new FedranHyphenPlugin("|");

    it("bobyobo", function(done)
    {
        const input = "bobyobo";
        let output = plugin.break(input);

        expect(output).toEqual("bo|byobo");
        done();
    });

    it("bobyobo dylan", function(done)
    {
        const input = "bobyobo dylan";
        let output = plugin.break(input);

        expect(output).toEqual("bo|byobo dylan");
        done();
    });

    it("Funikogo", function(done)
    {
        const input = "Funikogo";
        let output = plugin.break(input);

        expect(output).toEqual("Fu|ni|kogo");
        done();
    });

    it("Ganóshyo", function(done)
    {
        const input = "Ganóshyo";
        let output = plugin.break(input);

        expect(output).toEqual("Ga|nóshyo");
        done();
    });

    it("Funikogo Ganóshyo", function(done)
    {
        const input = "Funikogo Ganóshyo";
        let output = plugin.break(input);

        expect(output).toEqual("Fu|ni|kogo Ga|nóshyo");
        done();
    });

    it("house", function(done)
    {
        const input = "house";
        let output = plugin.break(input);

        expect(output).toEqual("house");
        done();
    });
});
